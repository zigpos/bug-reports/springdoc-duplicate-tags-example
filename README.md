# Springdoc Duplicate Tags Example

cf. [Springdoc Ticket 1625](https://github.com/springdoc/springdoc-openapi/issues/1625)

We'd like to structure our API description using tags.
For technical reasons, there are multiple controller classes which share a tag.
We'd also like to add a description to the tags for better UX.
However, we don't want to repeat the tag description in the controllers and would prefer to just declare it once.
We did it that way with Springfox and would like to do it again.

This causes problems with Springdoc, because the tags with descriptions (programmatically declared in `OpenApiConfig`) 
and the tag annotations on the controller classes are combined and result in duplicate
tag entries in the Open API document:

```json
{
  "tags": [
    {
      "name": "Positions v1",
      "description": "Legacy Positions API (v1)"
    },
    {
      "name": "Positions v1"
    }
  ]
}
```
(Note that the tag without description only exists once even though it is declared on two controllers)

Sometimes the Swagger UI will display the tag description, sometimes not, likely depending on the order in `tags`.
(This may not work reliably in this small example, but does occur in a real project)

Note that the [OpenAPI specification](https://swagger.io/specification/) states: *"Each tag name in the list MUST be unique."* 

Workarounds:
* post-process the OpenAPI object, see `OpenApiConfig.duplicateTagsFixer`
* repeat tag description in controllers (put it in a global constant to repeating the content)
