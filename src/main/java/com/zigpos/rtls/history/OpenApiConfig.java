package com.zigpos.rtls.history;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.tags.Tag;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

@Configuration(proxyBeanMethods = false)
class OpenApiConfig {

    public static class Tags {
        public static final String POSITIONS_V1 = "Positions v1";
        // + other tags

        private Tags() {
        }
    }

    @Bean
    OpenAPI openApiDescriptor() {
        var appVersion = "we read this at runtime from build output";

        var api = new OpenAPI();
        api.info(new Info()
                .title("Example App")
                .version(appVersion)
        );
        // + license, contact, external docs etc

        api.tags(List.of(
                new Tag().name(Tags.POSITIONS_V1).description("Legacy Positions API (v1)")
                // + descriptions for other tags
        ));

        return api;
    }

//    TODO uncomment this to fix duplicate tags
//    @Bean
    OpenApiCustomiser duplicateTagsFixer() {
        return new OpenApiCustomiser() {
            @Override
            public void customise(OpenAPI openApi) {
                var tagsByName = openApi.getTags().stream()
                        .collect(Collectors.groupingBy(Tag::getName));
                var bestTags = tagsByName.values().stream()
                        .map(this::selectBestTag)
                        .collect(Collectors.toList());
                openApi.setTags(bestTags);
            }

            private Tag selectBestTag(List<Tag> tags) {
                return tags.stream().reduce(this::selectTagWithNonNullDescription)
                        .orElseThrow(() -> new IllegalArgumentException("tags must not be empty"));
            }

            private Tag selectTagWithNonNullDescription(Tag left, Tag right) {
                if (left.getDescription() == null) {
                    return right;
                }
                return left;
            }
        };
    }

}
