package com.zigpos.rtls.history;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.zigpos.rtls.history.OpenApiConfig.Tags.POSITIONS_V1;

@Tag(name = POSITIONS_V1)
@RestController
@RequestMapping("v1/positions")
class PositionControllerV1 {

    @GetMapping
    Iterable<String> getPositionsV1() {
        return List.of("dummy", "example", "data");
    }

    // + lots of endpoints and DTO definitions
}
