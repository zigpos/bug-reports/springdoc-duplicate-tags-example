package com.zigpos.rtls.history;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.zigpos.rtls.history.OpenApiConfig.Tags.POSITIONS_V1;

@Tag(name = POSITIONS_V1)
@RestController
@RequestMapping("v1/positions/backup")
class PositionBackupControllerV1 {

    @GetMapping
    Iterable<String> createBackup() {
        return List.of("a", "zip", "file", "or", "something");
    }

    @PostMapping
    void loadBackup(/* + file upload */) {
        // process backup
    }

    // + DTO definitions for backups
}
